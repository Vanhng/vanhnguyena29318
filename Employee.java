/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baiTapTlu;

/**
 *
 * @author DELLXPS15
 */
public class Employee {
    private String firstName;
    private String lastName;
    private Double monthlySalary;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(Double monthlySalary) {      
            this.monthlySalary = monthlySalary;
    }
    public Employee(){
        this.firstName = "A";
        this.lastName = "B";
        this.monthlySalary = 0.0;
    }
    public Employee(String firstName, String lastName, Double monthlySalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.monthlySalary = monthlySalary;
    }
    public void displayYearlySalary(){
        System.out.println("Yearly Salary: "+monthlySalary*12);
    }
    public void displayYearlySalaryx10(){
        System.out.println("Yearly Salary after 10% raise: "+(monthlySalary*110/100)*12);
    }
}